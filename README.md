# Users-list

## About

Users/contacts list with fetch users data, search input filter and select user.

## How to run project locally

1. Clone repository
2. Run "npm install" in CLI
3. Run "npm start" in CLI
