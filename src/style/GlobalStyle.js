import { createGlobalStyle } from 'styled-components';
import '@fontsource/roboto';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font-family: 'Roboto', sans-serif;
  }

  input {
    border: 1px solid rgba(0,0,0,.5);
  }

  input[type='checkbox'] {
    cursor: pointer;
  }

  input:focus {
    outline: none
  }
`;

export default GlobalStyle;
