import axios from 'axios';

export const getUsers = async () => {
  try {
    const response = await axios.get(
      'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};
