import React from 'react';
import GlobalStyle from 'style/GlobalStyle';
import UsersList from 'components/molecules/UsersList';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  margin: 50px auto;
  max-width: 960px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const App = () => (
  <StyledWrapper>
    <GlobalStyle />
    <UsersList />
  </StyledWrapper>
);

export default App;
