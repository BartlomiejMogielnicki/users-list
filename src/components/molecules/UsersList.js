import React, { useState, useEffect } from 'react';
import UserItem from 'components/atoms/UserItem';
import FilterInput from 'components/atoms/FilterInput';
import { getUsers } from 'utils/index';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  text-align: center;
`;

const UsersList = () => {
  const [usersList, setUsersList] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await getUsers();

      if (users) {
        const updatedUsers = users.map((item) => {
          return {
            ...item,
            isSelected: false,
          };
        });

        setUsersList(updatedUsers);
      }
    };

    fetchUsers();
  }, []);

  useEffect(() => {
    const selectedUsers = [];

    usersList.forEach((item) => {
      if (item.isSelected) {
        selectedUsers.push(item.id);
      }
    });

    if (selectedUsers.length > 0) {
      console.log('Selected users ids: ' + selectedUsers);
    }
  }, [usersList]);

  const selectUser = (id) => {
    const userIndex = usersList.findIndex((item) => item.id === id);
    const backupList = usersList;

    backupList[userIndex].isSelected = !usersList[userIndex].isSelected;

    setUsersList([...backupList]);
  };

  return (
    <StyledWrapper>
      <h2>Contacts</h2>
      <FilterInput searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      <ul>
        {usersList &&
          usersList
            .filter((item) => {
              if (
                item.first_name.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return item;
              } else if (
                item.last_name.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return item;
              } else {
                return null;
              }
            })
            .sort((a, b) => a.last_name.localeCompare(b.last_name))
            .map((item) => (
              <UserItem key={item.id} user={item} selectUser={selectUser} />
            ))}
      </ul>
    </StyledWrapper>
  );
};

export default UsersList;
