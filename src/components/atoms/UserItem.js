import React from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.li`
  margin: 5px;
  width: 400px;
  height: 65px;
  display: grid;
  grid-template-columns: 100px 1fr 50px;
  align-items: center;
  background-color: #eee;
  border: 1px solid rgba(0, 0, 0, 0.5);
  border-radius: 5px;
  cursor: pointer;
`;

const StyledAvatar = styled.img`
  margin-left: 20px;
  width: 50px;
  height: 50px;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.5);
  border-radius: 50%;
`;

const UsersItem = ({
  selectUser,
  user: { id, avatar, first_name, last_name, isSelected },
}) => (
  <StyledWrapper onClick={() => selectUser(id)}>
    <StyledAvatar src={avatar} alt={`${first_name} ${last_name}`} />
    <p>{`${first_name} ${last_name}`}</p>
    <input type="checkbox" checked={isSelected} readOnly />
  </StyledWrapper>
);

export default UsersItem;
