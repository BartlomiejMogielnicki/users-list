import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
  margin: 5px;
  padding: 5px;
  width: 400px;
  border-radius: 5px;
`;

const FilterInput = ({ searchTerm, setSearchTerm }) => (
  <label htmlFor="filter">
    <StyledInput
      type="text"
      name="filter"
      onChange={(e) => setSearchTerm(e.target.value)}
      value={searchTerm}
      placeholder="Search by first or last name..."
    />
  </label>
);

export default FilterInput;
